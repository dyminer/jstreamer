package org.dyminer.jstreamer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.univocity.parsers.common.IterableResult;
import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvRoutines;
import com.univocity.parsers.csv.CsvWriterSettings;

public class CsvBeanUtils {
	
	public static <T> Stream<T> parseStream(File source, Class<T> type) throws FileNotFoundException{
		if(source==null || type==null) {
			throw new IllegalArgumentException();
		}
		
		//we open a valid InputStreamReader
		InputStreamReader reader = new InputStreamReader(
			new FileInputStream(source), StandardCharsets.UTF_8
		);
		
		//we set the csv parsing settings
		CsvParserSettings settings = new CsvParserSettings();
		settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");
		settings.setHeaderExtractionEnabled(true);
		settings.setReadInputOnSeparateThread(false);
		
		//we open a CSV strategy
		CsvRoutines parsingRoutine = new CsvRoutines(settings);
		
		//we get the first value
		IterableResult<T, ParsingContext> first = parsingRoutine.iterate(
			type, reader
		);
		
		//we build a stream using a spliterator built from an IterableResult (which
		//implements Iterator<T> interface).
		return StreamSupport.stream(first.spliterator(), false);
	}
	
	
	

	public static <T> void saveStream(File destination, Class<T> type, Stream<T> data) {
		if(destination==null || type==null || data==null) {
			throw new IllegalArgumentException();
		}
		
		// Then the output format
		CsvWriterSettings writerSettings = new CsvWriterSettings();
		writerSettings.getFormat().setLineSeparator("\r\n");
		writerSettings.getFormat().setDelimiter('\t');
		writerSettings.setQuoteAllFields(false);
		
		//we open a CSV strategy
		CsvRoutines parsingRoutine = new CsvRoutines(writerSettings);
		
		//write the content of the stream
		Iterable<T> iterable = data.collect(Collectors.toList());
		parsingRoutine.writeAll(
			iterable, type, destination
		);
		
	}
}

